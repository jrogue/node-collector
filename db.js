/*jslint node: true */
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/lamps', function () {
    console.log('mongodb connected');
});
module.exports = mongoose;