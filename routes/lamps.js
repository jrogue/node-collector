/*jslint node: true */
var Lamp = require('../models/lamp');

exports.addLamp = function (req, res, next) {
    var lamp = new Lamp({
        lampname: req.body.lampname,
        status: req.body.status
    });
    lamp.save(function (err, lamp) {
        if (err) { return next(err); }
        res.status(201).json(lamp);
    });
};

exports.updateLamp = function (req, res, next) {
    Lamp.findOne({ _id: req.params.id }, function (err, lamp) {
        if (err) { return next(err); }
        lamp.status = req.body.status;
        lamp.save(function (err, lamp) {
            if (err) { return next(err); }
            res.status(201).json(lamp);
        });
    });
};

exports.deleteLamp = function (req, res, next) {
    Lamp.remove({ _id: req.params.id }, function (err, lamp) {
        if (err) { return next(err); }
        res.status(201).json(lamp);
    });
};

exports.findAll = function (req, res, next) {
    Lamp.find(function (err, lamps) {
        if (err) { return next(err); }
        res.status(201).json(lamps);
    });
};
 
exports.findById = function (req, res, next) {
    Lamp.findOne({ _id: req.params.id }, function (err, lamp) {
        if (err) { return next(err); }
        res.status(201).json(lamp);
    });
};