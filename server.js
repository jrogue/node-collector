/*jslint node: true */
var express = require('express'),
    bodyParser = require('body-parser'),
    lamps = require('./routes/lamps.js');
 
var app = express();

app.use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Pass to next layer of middleware
    next();
});

app.post('/lamps', lamps.addLamp);
app.get('/lamps', lamps.findAll);
app.get('/lamps/:id', lamps.findById);
app.put('/lamps/:id', lamps.updateLamp);
app.delete('/lamps/:id', lamps.deleteLamp);

app.listen(3000);
console.log('Listening on port 3000...');