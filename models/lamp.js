/*jslint node: true */
var db = require('../db');

var Lamp = db.model('Lamp', {
    lampname:	{ type: String, required: true },
    status:	{ type: String, required: true },
    date:	{ type: Date, required: true, default: Date.now }
});
module.exports = Lamp;